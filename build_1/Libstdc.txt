###################  Libstdc  ####################

# Untar the package
tar zxvf gcc-7_7.2.0.orig.tar.gz

# Go to directory previously untar
cd gcc-7-7.2.0/

# Untar new package
tar Jxvf gcc-7.2.0.tar.xz
cd gcc-7.2.0

# Add build dir and go to it
mkdir build
cd build

# Prepapre compilation
../libstdc++-v3/configure           \
    --host=$LFS_TGT                 \
    --prefix=/tools                 \
    --disable-multilib              \
    --disable-nls                   \
    --disable-libstdcxx-threads     \
    --disable-libstdcxx-pch         \
    --with-gxx-include-dir=/tools/$LFS_TGT/include/c++/7.2.0

# Launch compilation
make

# Install package
make install
