###################  Eudev  ####################

# Untar the package and go into it
./extract.sh eudev-3.2.2.tar.gz && cd eudev-3.2.2

# Fix test script
sed -r -i 's|/usr(/bin/test)|\1|' test/udev-test.pl

# Remove uneed line that cause failure
sed -i '/keyboard_lookup_key/d' src/udev/udev-builtin-keyboard.c

# workaround problem form /tools
cat > config.cache << "EOF"
HAVE_BLKID=1
BLKID_LIBS="-lblkid"
BLKID_CFLAGS="-I/tools/include"
EOF

# Prepapre compilation
./configure --prefix=/usr           \
            --bindir=/sbin          \
            --sbindir=/sbin         \
            --libdir=/usr/lib       \
            --sysconfdir=/etc       \
            --libexecdir=/lib       \
            --with-rootprefix=      \
            --with-rootlibdir=/lib  \
            --enable-manpages       \
            --disable-static        \
            --config-cache

# Launch compilation
LIBRARY_PATH=/tools/lib make

# Create dir for test
mkdir -pv /lib/udev/rules.d
mkdir -pv /etc/udev/rules.d

# Launche test
make LD_LIBRARY_PATH=/tools/lib check

# Install
make LD_LIBRARY_PATH=/tools/lib install

# Install custom rule for lfs
tar -xvf ../udev-lfs-20140408.tar.bz2
make -f udev-lfs-20140408/Makefile.lfs install

# Create inode database
LD_LIBRARY_PATH=/tools/lib udevadm hwdb --update
