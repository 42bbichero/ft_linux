###################  Vim  ####################

# Untar the package and go into it
./extract.sh vim-8.0.586.tar.bz2 && cd vim-8.0.586

# Change location of vimrc file
echo '#define SYS_VIMRC_FILE "/etc/vimrc"' >> src/feature.h

# remove a test that fail
sed -i '/call/{s/split/xsplit/;s/303/492/}' src/testdir/test_recover.vim

# Prepapre compilation
./configure --prefix=/usr

# Launch compilation
make -j1 test &> vim-test.log

# Create symlink vim for vi
ln -sv vim /usr/bin/vi
for L in  /usr/share/man/{,*/}man1/vim.1; do
    ln -sv vim.1 $(dirname $L)/vi.1
done

# Create symlink for documentation
ln -sv ../vim/vim80/doc /usr/share/doc/vim-8.0.586
