###################  ACL  ####################

# Untar the package and go into it
./extract.sh acl-2.2.52.src.tar.gz && cd acl-2.2.52

# Make documentation versioned
sed -i -e 's|/@pkg_name@|&-@pkg_version@|' include/builddefs.in

# Fix broken test
sed -i "s:| sed.*::g" test/{sbits-restore,cp,misc}.test
sed -i 's/{(/\\{(/' test/run
sed -i -e "/TABS-1;/a if (x > (TABS-1)) x = (TABS-1);" \
    libacl/__acl_to_any_text.c

# Prepapre compilation
./configure --prefix=/usr    \
            --bindir=/bin    \
            --disable-static \
            --libexecdir=/usr/lib

# Launch compilation
make

# Install package
make install install-dev install-lib
chmod -v 755 /usr/lib/libacl.so

# Move shared library to /lib
mv -v /usr/lib/libacl.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libacl.so) /usr/lib/libacl.so
