###################  Gettext  ####################

# Untar the package and go into it
./extract.sh gettext-0.19.8.1.tar.xz && cd gettext-0.19.8.1

# Delete to boggy test
sed -i '/^TESTS =/d' gettext-runtime/tests/Makefile.in &&
sed -i 's/test-lock..EXEEXT.//' gettext-tools/gnulib-tests/Makefile.in

# Prepapre compilation
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/gettext-0.19.8.1

# Launch compilation, check and installatiokn
make && make check && make install
chmod -v 0755 /usr/lib/preloadable_libintl.so
