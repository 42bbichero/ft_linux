###################  Expat  ####################

# Untar the package and go into it
./extract.sh expat-2.2.3.tar.bz2 && cd expat-2.2.3

# Fix problem with lfs
sed -i 's|usr/bin/env |bin/|' run.sh.in

# Prepapre compilation
./configure --prefix=/usr --disable-static

# Launch compilation, check and installatiokn
make && make check && make install

# Install documentation
install -v -dm755 /usr/share/doc/expat-2.2.3
install -v -m644 doc/*.{html,png,css} /usr/share/doc/expat-2.2.3
