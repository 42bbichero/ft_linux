###################  Shadow  ####################

# Untar the package and go into it
./extract.sh shadow-4.5.tar.xz && cd shadow-4.5

# Disable install of groups program
sed -i 's/groups$(EXEEXT) //' src/Makefile.in
find man -name Makefile.in -exec sed -i 's/groups\.1 / /'   {} \;
find man -name Makefile.in -exec sed -i 's/getspnam\.3 / /' {} \;
find man -name Makefile.in -exec sed -i 's/passwd\.5 / /'   {} \;

# Use secure sha-512 method
sed -i -e 's@#ENCRYPT_METHOD DES@ENCRYPT_METHOD SHA512@' \
       -e 's@/var/spool/mail@/var/mail@' etc/login.defs

# Make useradd consistent
sed -i 's/1000/999/' etc/useradd

# Prepapre compilation
./configure --sysconfdir=/etc --with-group-name-max-length=32

# Launch compilation and installatiokn
make && make install

# Move misplaced file
mv -v /usr/bin/passwd /bin

# Enable shadowed, and group shadowed password
pwconv
grpconv

# Define root password
passwd root 
