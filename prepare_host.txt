########################  HOST  #########################

# Host need :
- 4 vpcu
- 1Gb of RAM
- 2 virtual disk: one with basic partitioning and other for ft_linux with /, /home, /boot and swap
- host need a bios grub partition on firt disk for booting ft_linux

## Config for current ft_linux
# Debian 9.3 VM with 20GB (/dev/sda) for the first disk (LVM)
- Second disk have 22GB (/dev/sdb)

# create bios boot grub partition
fdisk /dev/sda
n p 4 +100M
w

# Create all partition on second disk
fdisk /dev/sdb
n p 1 +8G (root partition)
n p 2 +10G (home)
n p 3 +100M (boot)
n p 4 +1G (swap)
w

# Erase and create filsystem on all partition
mkfs.ext2 /dev/sda4
mkfs.ext4 /dev/sdb1
mkfs.ext4 /dev/sdb2
mkfs.ext2 /dev/sdb3
mkswap /dev/sdb4

# host need to have fews package to start LFS
# all package must be install with specific version

sudo apt install --no-install-recommends bash binutils bison bzip2 coreutils diffutils findutils gawk gcc make grep gzip m4 patch perl sed tar xz-utils g++ texinfo

# To see if system is ready (have good version and can compile) run check-version.sh file
sh check-version.sh

# If /bin/sh don't link to /bin/bash :
rm /bin/sh
ln -s /bin/bash /bin/sh

# then re-run check-version.sh

# Create env var for root partition of ft_linux
mkdir /mnt/lfs
mount -t ext4 /dev/sdb1 /mnt/lfs

# Export it in bashrc
echo "export LFS=/mnt/lfs" > ~/.bashrc && source ~/.bashrc

# Mount other partition previously created
mkdir $LFS/home
mkdir $LFS/boot
mount -t ext2 /dev/sdb4 /mnt/lfs/boot/
mount -t ext4 /dev/sdb2 /mnt/lfs/home/

# Active swap
swapon -v /dev/sdb3
