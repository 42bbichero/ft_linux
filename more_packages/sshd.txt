######################  SSHD  ######################

## SSH required OpenSSL lib

# Extract openssh and go to dir
./extract.sh openssl-1.1.0f.tar.gz
cd openssl-1.1.0f

# Prepare compilation
./config --prefix=/usr         \
         --openssldir=/etc/ssl \
         --libdir=lib          \
         shared                \
         zlib-dynamic

# Compile
make

# Launch tests
make test

# Disable static lib
sed -i 's# libcrypto.a##;s# libssl.a##;/INSTALL_LIBS/s#libcrypto.a##' Makefile

# Install
make MANSUFFIX=ssl install           &&
mv -v /usr/share/doc/openssl{,-1.1.0f} &&
cp -vfr doc/* /usr/share/doc/openssl-1.1.0f

# Extract openssh package
./extract.js openssh-7.5p1.tar.gz
cd openssh-7.5p1

# Set up proper sshd environment
install  -v -m700 -d /var/lib/sshd &&
chown    -v root:sys /var/lib/sshd &&

groupadd -g 50 sshd        &&
useradd  -c 'sshd PrivSep' \
         -d /var/lib/sshd  \
         -g sshd           \
         -s /bin/false     \
         -u 50 sshd

# copy scp to run test
cp scp /usr/bin
make tests

# Install
make install &&
install -v -m755    contrib/ssh-copy-id /usr/bin     &&

install -v -m644    contrib/ssh-copy-id.1 \
                    /usr/share/man/man1              &&
install -v -m755 -d /usr/share/doc/openssh-7.5p1     &&
install -v -m644    INSTALL LICENCE OVERVIEW README* \
                    /usr/share/doc/openssh-7.5p1
