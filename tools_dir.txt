#################  TOOLS DIR  #################

# Create tools dir to keep in separate dir all tools compiled 
# used while the instalation of all package to create out LFS

# Create the directory
mkdir -v $LFS/tools

# Create symbolic link so that /tools refer to $LFS/tools
ln -sv $LFS/tools /

# Create non-root user to have a cleaner env and minimise damage
groupadd lfs

# Configure user
useradd -s /bin/bash -g lfs -m -k /dev/null lfs
passwd lfs

# Make lfs owner of lfs tools
chown -v lfs $LFS/tools

# And give lfs all permision on all sub directory / file created in
chown -v lfs $LFS/sources

# Now log as lfs user
su - lfs

## Set up env
# create bash_profile file with env var
cat > ~/.bash_profile << "EOF"
exec env -i HOME=$HOME TERM=$TERM PS1='\u:\w\$ ' /bin/bash
EOF

# Our new environment need to read a .bashrc file (non-login shell)
cat > ~/.bashrc << "EOF"
set +h
umask 022
LFS=/mnt/lfs
LC_ALL=POSIX
LFS_TGT=$(uname -m)-lfs-linux-gnu
PATH=/tools/bin:/bin:/usr/bin
export LFS LC_ALL LFS_TGT PATH
EOF

# Now source the newly file
source ~/.bash_profile
