###################  Expect  ###################

# Untar the package
tar xzvf expect5.45.tar.gz

# Go to directory previously untar
cd expect5.45

# Force Expect's configure script to use /bin/stty
cp -v configure{,.orig}
sed 's:/usr/local/bin:/bin:' configure.orig > configure

# Prepapre compilation
./configure --prefix=/tools       \
            --with-tcl=/tools/lib \
            --with-tclinclude=/tools/include

# Launch compilation
make

# Install package
make install

# Launch tests
make SCRIPTS="" install
