###################    ####################

# Untar the package
tar xzvf bash-4.4.tar.gz

# Go to directory previously untar
cd bash-4.4

# Prepapre compilation
./configure --prefix=/tools --without-bash-malloc

# Launch compilation and launch test
make
make tests

# Install package
make install

# Create link for program that use sh
ln -sv bash /tools/bin/sh
