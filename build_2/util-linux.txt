###################  Util Linux  ####################

# Untar the package
tar Jxvf util-linux-2.30.1.tar.xz

# Go to directory previously untar
cd util-linux-2.30.1

# Prepapre compilation
./configure --prefix=/tools                \
            --without-python               \
            --disable-makeinstall-chown    \
            --without-systemdsystemunitdir \
            --without-ncurses              \
            PKG_CONFIG=""

# Launch compilation
make

# Install package
make install
