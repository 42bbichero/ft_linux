###################  Textinfo  ####################

# Untar the package
tar Jxvf texinfo-6.4.tar.xz

# Go to directory previously untar
cd texinfo-6.4

# Prepapre compilation
./configure --prefix=/tools

# Launch compilation and test
make && make check

# Install package
make install
