###################  PACKAGES  ###############

# Create directory to stock all package downloaded
mkdir -v $LFS/sources

# Make directory stiky (only file owner can delete his file)
chmod -v a+wt $LFS/sources

# Download file with all package link
wget http://www.linuxfromscratch.org/lfs/view/stable/wget-list

# Donwload all package with wget and last downloaded file
wget --input-file=wget-list --continue --directory-prefix=$LFS/sources

# Download patch for bugfic package
